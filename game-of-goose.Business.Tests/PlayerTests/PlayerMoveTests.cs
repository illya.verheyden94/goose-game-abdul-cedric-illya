﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using Moq;


namespace game_of_goose.Business.Tests.PlayerTests
{
    public class PlayerMoveTests
    {
        [Test]
        public void WhenPlayerRollsTheDice_ThenPlayerMovesByDiceAmount()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 5;
            int[] diceRoll = [1, 2];
            int expectedResult = 8;

            //Act
            player.Move(diceRoll);

            //Assert
            Assert.That(expectedResult, Is.EqualTo(player.Position));
        }

        [Test]
        public void WhenPlayerRollsPast63_ThenTheyMoveBackByRemainingDiceAmount()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 62;
            int[] diceRoll = [6, 3];
            int expectedResult = 55;

            //Act
            player.Move(diceRoll);

            //Assert
            Assert.That(expectedResult, Is.EqualTo(player.Position));
        }
    }
}
