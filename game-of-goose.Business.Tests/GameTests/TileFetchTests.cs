﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using Moq;

namespace game_of_goose.Business.Tests.GameTests
{
    public class TileFetchTests
    {
        [Test]
        public void WhenFetchingTileOnPosition6_ThenWeExpectATileOfTypeBridge()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            int tilePosition = 6;
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            ITile expectedTile = new Bridge(tilePosition, logger.Object);

            //Act
            game.GetTile(tilePosition);

            //Assert
            Assert.That(game.GetTile(tilePosition).GetType(), Is.EqualTo(expectedTile.GetType()));
        }
    }
}
