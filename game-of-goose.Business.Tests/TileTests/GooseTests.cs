﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using Moq;

namespace game_of_goose.Business.Tests.TileTests
{
    public class GooseTests
    {
        [Test]

        public void WhenPlayerEntersGooseTile5_ThenTheGoose5MovesThemForwardBasedOnTheDiceRollValue()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 7;
            player.Turn = 1;
            int[] diceRollValue = [1, 1];
            int expectedResult = 11;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }

        [TestCase(4, 5, 0, 26)]
        [TestCase(5, 4, 0, 26)]
        [TestCase(6, 3, 0, 53)]
        [TestCase(3, 6, 0, 53)]
        [TestCase(3, 2, 0, 10)]
        public void WhenPlayerThrows5and4Or6and3OnFirstTurn_ThenHeMovesTo26or53Respectively(int diceRollValueOne, int diceRollValueTwo, int playerTurn, int expectedResult)
        {
            //Arrange 
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Turn = playerTurn;
            int[] diceRollValue = [diceRollValueOne, diceRollValueTwo];

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }
        [Test]
        public void WhenPlayerRolls4WhenStandingOn10_ThenHeEndsUpAt22()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 10;
            int[] diceRollValue = [2, 2];
            int expectedResult = 22;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));

        }
        [Test]
        public void WhenPlayerRolls10WhenStandingOn57_ThenHeEndsUpAt49()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 57;
            int[] diceRollValue = [5, 5];
            int expectedResult = 49;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }
        [Test]
        public void WhenPlayerRolls5whenStandingOn49_ThenHeShouldEndUpInSpace62()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 49;
            player.Turn = 9;
            int[] diceRollValue = [2, 3];
            int expectedResult = 62;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }

        [Test]
        public void WhenPlayerRolls8WhenStandingOn51_ThenHeEndsUpAt51()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Turn = 9;
            player.Position = 51;
            int[] diceRollValue = [4, 4];
            int expectedResult = 51;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }
    }
}
