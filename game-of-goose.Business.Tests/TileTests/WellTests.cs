﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using Moq;

namespace game_of_goose.Business.Tests.TileTests
{
    public class WellTests
    {
        [Test]

        public void WhenPlayer2EntersWell_ThenPlayer1IsNotStuck()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 28;
            int[] diceRollValuePlayer1 = [1, 2];
            IPlayer player2 = new Player(game, "Bart", logger.Object);
            player2.Position = 29;
            int[] diceRollValuePlayer2 = [1, 1];
            var expectedResult = false;

            //Act
            player.Move(diceRollValuePlayer1);
            player2.Move(diceRollValuePlayer2);
            player.Move(diceRollValuePlayer1);

            //Assert
            Assert.That(player.StuckInWell, Is.EqualTo(expectedResult));
        }

        [Test]
        public void WhenPlayer1IsStuck_ThenHeCanNotLeaveTheWell()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 28;
            int[] diceRollValuePlayer1 = [1, 2];
            int expectedResult = 31;

            //Act
            player.Move(diceRollValuePlayer1);
            player.Move(diceRollValuePlayer1);


            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));

        }
        [Test]
        public void WhenPlayer2EntersWell_ThenPlayer1CanMoveToTile34()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 28;
            int[] diceRollValuePlayer1 = [1, 2];
            IPlayer player2 = new Player(game, "Tim", logger.Object);
            player2.Position = 29;
            int[] diceRollValuePlayer2 = [1, 1];
            int expectedResult = 34;

            //Act
            player.Move(diceRollValuePlayer1);
            player2.Move(diceRollValuePlayer2);
            player.Move(diceRollValuePlayer1);

            //Assert
            Assert.That(player.Position, Is.EqualTo(expectedResult));
        }
    }
}
