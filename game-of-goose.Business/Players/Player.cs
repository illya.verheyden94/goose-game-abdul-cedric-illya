﻿using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Tiles;

namespace game_of_goose.Business.Players
{
    public class Player : IPlayer
    {
        private int _destination;
        private ILogger _logger;
        public Player(IGame game, string name, ILogger logger)
        {
            Position = 0;
            Game = game;
            StuckInWell = false;
            Name = name;
            _logger = logger;
            DiceRolls = [0, 0];
        }
        public int Position { get; set; }
        public int Turn { get; set; }
        public IGame Game { get; set; }
        public int[] DiceRolls { get; set; }
        public string Name { get; set; }
        public bool StuckInWell { get; set; }
        public bool OppositeDirection { get; set; }
        public void AddTurnCount(int turns)
        {
            Turn += turns;
        }
        public void Move(int[] diceRolls)
        {
            DiceRolls = diceRolls;
            OppositeDirection = false;
            _destination = Position;
            CheckIfStuckInWell();
            CheckIfMovingOpposite();
            _logger.Log($"{this.Name} gooit {this.DiceRolls.Sum()}.");
            MoveTo(_destination);
        }
        private void CheckIfMovingOpposite()
        {
            if (_destination > Game.BoardSize)
            {
                OppositeDirection = true;
                int difference = _destination - Game.BoardSize;
                _destination = Game.BoardSize - difference;
            }
        }
        private void CheckIfStuckInWell()
        {
            if (!StuckInWell)
            {
                _destination = Position + DiceRolls.Sum();
            }
        }
        public void MoveTo(int destination)
        {
            Position = destination;
            ITile tile = Game.GetTile(destination);
            tile.CheckPosition(this);
        }
        public void WinsGame(IPlayer player)
        {
            Game.GameWon(player);
        }
    }
}
