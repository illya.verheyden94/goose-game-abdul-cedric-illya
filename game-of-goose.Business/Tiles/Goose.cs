﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Goose : Tile
    {
        private int _destination;
        private bool _firstTurnSendsPlayer;
        public Goose(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            int totalDiceRollValue = player.DiceRolls.Sum();
            _destination = player.Position + totalDiceRollValue;
            CheckConditions(player, _destination, totalDiceRollValue);
            player.MoveTo(_destination);
        }
        private void CheckConditions(IPlayer player, int newDestination, int totalDiceRollValue)
        {
            if (player.Turn == 0)
            {
                CheckIfRollsAre6and3(player);
                CheckIfRollsAre5and4(player);
            }
            if (_firstTurnSendsPlayer == false)
            {
                CheckIfOppositeDirection(player, newDestination, totalDiceRollValue);
            }
        }
        private void CheckIfOppositeDirection(IPlayer player, int newDestination, int totalDiceRollValue)
        {
            if (player.OppositeDirection == true)
            {
                SetDestinationBackwards(player, totalDiceRollValue);
            }
            else
            {
                SetDestinationForward(player, newDestination);
            }
        }
        private void SetDestinationForward(IPlayer player, int newDestination)
        {
            CheckIfGoingPast63(player, newDestination);
            _logger.Log($"{player.Name} komt op plek {Position}. Hier staat een gevaarlijke gans. Hij stuurt je naar het plekje {newDestination}");

        }
        private void CheckIfGoingPast63(IPlayer player, int newDestination)
        {
            if (newDestination > 63)
            {
                int difference = newDestination - 63;
                newDestination = 63 - difference;
                player.OppositeDirection = true;
            }
            _destination = newDestination;
        }
        private void SetDestinationBackwards(IPlayer player, int totalDiceRollValue)
        {
            int newDestination = player.Position - totalDiceRollValue;
            _logger.Log($"{player.Name} komt op plek {Position}. Hier staat een gevaarlijke gans. Hij stuurt je naar plekje {newDestination}");
            _destination = newDestination;
        }
        private void CheckIfRollsAre5and4(IPlayer player)
        {
            if (player.DiceRolls.Contains(5) && player.DiceRolls.Contains(4))
            {
                _logger.Log($"{player.Name} komt op plek {Position}. Aangezien je een {player.DiceRolls[0]} en een {player.DiceRolls[1]} hebt gerold, ben je het naar vakje 26 afgebold.");
                _destination = 26;
                _firstTurnSendsPlayer = true;
            }
        }
        private void CheckIfRollsAre6and3(IPlayer player)
        {
            if (player.DiceRolls.Contains(6) && player.DiceRolls.Contains(3))
            {
                _logger.Log($"{player.Name} komt op plek {Position}. Aangezien je een {player.DiceRolls[0]} en een {player.DiceRolls[1]} hebt gerold, ben je het naar vakje 53 afgebold.");
                _destination = 53;
                _firstTurnSendsPlayer = true;
            }
        }
    }
}
