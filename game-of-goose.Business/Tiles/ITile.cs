﻿using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public interface ITile
    {
        public int Position { get; set; }
        void CheckPosition(IPlayer player);
    }
}
