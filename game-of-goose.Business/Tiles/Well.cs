﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Well : Tile
    {
        public Well(int position, ILogger logger) : base(position, logger)
        {

        }
        private IPlayer TrappedPlayer { get; set; }
        public override void CheckPosition(IPlayer player)
        {
            if (TrappedPlayer == null)
            {
                PutFirstPlayerInWell(player);
            }
            else
            {
                SwapWithPlayerInWell(player);
            }
        }

        private void SwapWithPlayerInWell(IPlayer player)
        {
            if (player.GetHashCode() == TrappedPlayer.GetHashCode())
            {
                _logger.Log($"{player.Name} zit vast in de vergeetput. Hij blijft verderspelen met de dobbelstenen tot iemand komt.");
            }
            else
            {
                _logger.Log($"{player.Name} komt op plek {Position}. Dit is de vergeetput, {TrappedPlayer.Name} klautert er uit en jij valt er in. Wat een pech, tot als iemand er anders in valt, kan je niet meer weg.");
                TrappedPlayer.StuckInWell = false;
                player.StuckInWell = true;
                TrappedPlayer = player;
            }
        }
        private void PutFirstPlayerInWell(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Dit is de vergeetput, de knoerifast klautert er uit en jij valt er in. Wat een pech, tot als iemand er anders in valt, kan je niet meer weg.");
            player.StuckInWell = true;
            TrappedPlayer = player;
        }
    }
}