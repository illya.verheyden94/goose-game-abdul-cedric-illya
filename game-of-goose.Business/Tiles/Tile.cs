﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Tile : ITile
    {
        protected ILogger _logger;
        public Tile(int position, ILogger logger)
        {
            Position = position;
            _logger = logger;
        }
        public int Position { get; set; }
        public virtual void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Dit is een saai vakje.");
        }
    }
}
