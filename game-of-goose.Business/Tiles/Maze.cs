﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Maze : Tile
    {
        public Maze(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Dit is het doolhof, je bent verdwaald.");
            player.MoveTo(39);
        }
    }
}
