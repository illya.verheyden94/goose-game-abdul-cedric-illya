﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Inn : Tile
    {
        public Inn(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Je bevind jezelf in een herberg, sla een beurt over en werp een dwerg.");
            player.AddTurnCount(1);
        }
    }
}
