﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;


namespace game_of_goose.Business.Tiles
{
    public class Bridge : Tile
    {
        public Bridge(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op vakje {Position}. Je komt aan een brug, je loopt erover vliegensvlug.");
            player.MoveTo(12);
        }
    }
}
