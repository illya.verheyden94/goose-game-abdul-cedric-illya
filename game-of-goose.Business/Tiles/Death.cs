﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Death : Tile
    {
        public Death(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Je bent nu dood. Met het schaamrood op je billetjes bloot, begin je terug helemaal vooraan in de goot.");
            player.MoveTo(0);
        }
    }
}
