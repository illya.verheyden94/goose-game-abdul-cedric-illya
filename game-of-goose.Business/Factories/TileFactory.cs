﻿using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Tiles;
using game_of_goose.Business.Tiles.Enum;

namespace game_of_goose.Business.Factories
{
    public class TileFactory : ITileFactory
    {
        private ILogger logger;
        public TileFactory(ILogger logger)
        {
            this.logger = logger;
        }
        public ITile Create(int position, TileType tileType)
        {

            switch (tileType)
            {
                case TileType.Bridge:
                    return new Bridge(position, logger);

                case TileType.Inn:
                    return new Inn(position, logger);

                case TileType.Maze:
                    return new Maze(position, logger);

                case TileType.Death:
                    return new Death(position, logger);

                case TileType.Well:
                    return new Well(position, logger);

                case TileType.End:
                    return new End(position, logger);

                case TileType.Prison:
                    return new Prison(position, logger);

                case TileType.Goose:
                    return new Goose(position, logger);

                case TileType.RegularTile:
                    return new Tile(position, logger);

                default: throw new ArgumentException(nameof(tileType));
            }
        }
    }
}
