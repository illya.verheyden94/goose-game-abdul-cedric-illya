﻿using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Factories
{
    public class PlayerFactory : IPlayerFactory
    {
        public IPlayer Create(string playerName, IGame game, ILogger logger)
        {
            return new Player(game, playerName, logger);
        }
    }
}
