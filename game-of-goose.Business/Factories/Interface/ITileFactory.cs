﻿using game_of_goose.Business.Tiles;
using game_of_goose.Business.Tiles.Enum;

namespace game_of_goose.Business.Factories.Interface
{
    public interface ITileFactory
    {
        ITile Create(int position, TileType tileType);
    }
}
