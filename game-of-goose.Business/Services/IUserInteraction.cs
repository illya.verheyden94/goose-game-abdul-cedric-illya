﻿namespace game_of_goose.Business.Services
{
    public interface IUserInteraction
    {
        string GetUserInput();
        public void MakeUserPressEnter();
        public void ClearScreen();
    }
}
