﻿using game_of_goose.Business.Players;
using game_of_goose.Business.Tiles;

namespace game_of_goose.Business.GameState
{
    public interface IGame
    {
        int BoardSize { get; set; }
        bool GameIsWon { get; set; }
        void AskHowManyPlayers();
        void CreateBoard();
        IPlayer CreatePlayer(int spelerNummer);
        void GameWon(IPlayer player);
        ITile GetTile(int destination);
        void PlayGame();
        void PlayRound(IPlayer player);
        void ThanksForPlaying();
        void Welcome();
    }
}