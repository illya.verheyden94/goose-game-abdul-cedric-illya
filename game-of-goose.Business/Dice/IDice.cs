﻿namespace game_of_goose.Business.Dice
{
    public interface IDice
    {
        int RollDice(int maxSides = 6);
        int[] RollTheDices(int amount = 2);
    }
}