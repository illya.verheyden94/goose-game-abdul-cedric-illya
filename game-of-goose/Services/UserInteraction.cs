﻿using game_of_goose.Business.Services;

namespace game_of_goose.Services
{
    public class UserInteraction : IUserInteraction
    {
        public void ClearScreen()
        {
            Console.Clear();
        }

        public string GetUserInput()
        {
            return Console.ReadLine();
        }
        public void MakeUserPressEnter()
        {
            Console.ReadLine();
        }
    }
}
