﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Services;
namespace game_of_goose.Services
{
    public class ValidationService : IValidationService
    {
        public int ValidatePlayerAmount(int playersAmount, IUserInteraction userInteraction, ILogger logger)
        {
            while (!int.TryParse(userInteraction.GetUserInput(), out playersAmount) || playersAmount > 4 || playersAmount < 1)
            {
                logger.Log("Foute input. Minimaal 1, maximaal 4 spelers.");
            }
            return playersAmount;
        }
        public string ValidatePlayerName(string playerName, IUserInteraction userInteraction, ILogger logger)
        {
            while (String.IsNullOrWhiteSpace(playerName = userInteraction.GetUserInput()))
            {
                logger.Log($"U moet een naam ingeven, deze mag niet leeg zijn");
            }
            return playerName;
        }
    }
}
