﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using game_of_goose.Logging;
using game_of_goose.Services;
using Microsoft.Extensions.DependencyInjection;

namespace game_of_goose
{
    public class Startup
    {
        public ServiceCollection RegisterServices()
        {
            ServiceCollection services = new ServiceCollection();
            services.AddScoped<ILogger, ConsoleLogger>();
            services.AddScoped<List<ITile>, List<ITile>>();
            services.AddScoped<List<IPlayer>, List<IPlayer>>();
            services.AddScoped<ITileFactory, TileFactory>();
            services.AddScoped<IPlayerFactory, PlayerFactory>();
            services.AddScoped<IUserInteraction, UserInteraction>();
            services.AddScoped<IDice, Die>();
            services.AddScoped<IValidationService, ValidationService>();
            services.AddSingleton<IGame, Game>();
            return services;
        }
    }
}
